//approximation: reliable hardware for f()

while true
{
  k' = f(k);
  if (k - k' < e)
  {
    break;
  }
}

forall x: f(x) <= x;
forall x: f(x) >= 0;
